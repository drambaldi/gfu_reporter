package gfureporter

/**
 * @author davide
 * Created: Mon Jan 20 13:49:02 CET 2014
 */

import org.apache.commons.cli.Option

import org.fusesource.jansi.AnsiConsole
import static org.fusesource.jansi.Ansi.*
import static org.fusesource.jansi.Ansi.Color.*
import static org.fusesource.jansi.Ansi.Attribute.*

import gfureporter.fastqc.*

class GfuReporter
{
    public static String report_type

    public static void main(String[] args)
    {
        def date = new Date(Long.parseLong(System.getProperty("gfureporter.builddate")))

        // CLI BUILDER
        def cli = new CliBuilder(
            usage: "gfu-reporter <report_type> <samples>",
            header: "\nAvailable options (use -h for help):\n",
            footer: "\nVersion: ${System.getProperty("gfureporter.version")}, Build date: ${date}\n",
            posix:  true,
            width:  120
        )

        cli.with {
            h   longOpt: 'help'     , 'Usage Information', required: false
        }

        def opt = cli.parse(args)
        if ( !opt ) System.exit(1)

        if ( opt.h || !opt.arguments() ) {
            cli.usage()
            System.exit(1)
        }

        // GET report type (first non options argument) and remove it from list
        def extraArguments = opt.arguments()
        report_type = extraArguments.remove(0)

        switch(report_type)
        {
            case "fastqc":
                fastqc.Parser.parseProject(extraArguments)
            break
        }
    }

    // COLORS
    static org.fusesource.jansi.Ansi bold(String s)
    {
        ansi().a(INTENSITY_BOLD).a(s).reset()
    }

    static org.fusesource.jansi.Ansi green(String s)
    {
        ansi().fg(GREEN).a(s).reset()
    }

    static org.fusesource.jansi.Ansi greenbold(String s)
    {
        ansi().fg(GREEN).a(INTENSITY_BOLD).a(s).reset()
    }

    static org.fusesource.jansi.Ansi red(String s)
    {
        ansi().fg(RED).a(s).reset()
    }

    static org.fusesource.jansi.Ansi redbold(String s)
    {
        ansi().fg(RED).a(INTENSITY_BOLD).a(s).reset()
    }

    static org.fusesource.jansi.Ansi magenta(String s)
    {
        ansi().fg(MAGENTA).a(s).reset()
    }

    static org.fusesource.jansi.Ansi yellow(String s)
    {
        ansi().fg(YELLOW).a(s).reset()
    }
}
