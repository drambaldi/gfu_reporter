package gfureporter.fastqc

/**
 * @author davide
 * Created: Mon Jan 20 15:16:53 CET 2014
 */

public class Project
{
    public String name
    public ArrayList samples

    Project()
    {
        samples = new ArrayList()
    }

    String toString()
    {
        StringBuffer res = new StringBuffer()
        res << "PROJECT: $name.\n"
        samples.each { sample ->
            res << "\t" << sample.toString()
        }
        return res
    }

    String samplesToHtmlTable()
    {
        StringBuffer res = new StringBuffer()
        res << "<table>"
        samples.each { sample ->
            sample.read_groups.each { read_group ->
                res << "<tr>"
                res << "<td>${sample.name}</td>"
                res << "<td>${read_group.name}</td>"
                res << "<td>${read_group.fileName}</td>"
                res << "<td>${read_group.reads}</td>"
                res << "<td>${read_group.gc}</td>"
                res << "</tr>"
            }
        }
        res << "</table>"
        return res
    }

    String samplesToCsv()
    {
        StringBuffer res = new StringBuffer()
        res << "SampleID,FileType,FileName,Reads,GC%\n"
        samples.each { sample ->
            sample.read_groups.each { read_group ->
                res << "${sample.name},${read_group.name},${read_group.fileName},${read_group.reads},${read_group.gc}\n"
            }
        }
        return res
    }
}
