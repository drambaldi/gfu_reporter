package gfureporter.fastqc

import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.util.regex.Matcher
import java.util.regex.Pattern
import java.util.zip.*

import groovy.text.SimpleTemplateEngine

/**
 * @author davide
 * Created: Mon Jan 20 15:23:40 CET 2014
 */
class Parser
{
    public static gfureporter.fastqc.Project project

    static void parseProject(def samples)
    {
        project = new gfureporter.fastqc.Project()

        samples.each { sample ->
            parseZippedSample(sample)
        }
        println "HOME: ${System.getProperty("gfureporter.home")}"
        def text = new File ("${System.getProperty("gfureporter.home")}/templates/fastqc_summary.html")
        def engine = new SimpleTemplateEngine()

        def binding = [
            "projectName"  : project.name,
            "projectTable" : project.samplesToHtmlTable()
        ]
        def template = engine.createTemplate(text).make(binding)

        // DEBUG
        println "------------------------------"
        println project.toString()
        println "------------------------------"
        println template.toString()
        println "------------------------------"
        println project.samplesToCsv()
    }

    public static void parseZippedSample(String filename)
    {
        ZipInputStream zis
        InputStream  ins

        gfureporter.fastqc.Sample sample = new gfureporter.fastqc.Sample()

        try
        {
            ZipFile zipFile = new ZipFile(new File(filename))
            // FIRST PARSE SampleSheet.csv
            zipFile.entries().each { entry ->
                if (entry.getName().endsWith("SampleSheet.csv"))
                {
                    InputStream instream = zipFile.getInputStream(entry)
                    List lines = instream.readLines()
                    def samples = []
                    String[] headers = lines.remove(0).split(",")
                    lines.each { line ->
                        // skip empty lines
                        if (!line.trim().empty)
                        {
                            def sampleline = line.split(",")
                            def sample_map = [:]
                            headers.eachWithIndex { header, i ->
                                sample_map.put(header, sampleline[i])
                            }
                            samples << sample_map
                        }
                    }
                    if (project.name == null) project.name = samples[0]["SampleProject"]
                    sample.name = samples[0]["SampleID"]
                }
            }

            // SUB ZIP FILES
            for(Enumeration e = zipFile.entries();e.hasMoreElements();)
            {
                ZipEntry ze = (ZipEntry) e.nextElement()
                // FASTQC ZIP
                if(ze.getName().endsWith(".zip"))
                {
                    ins = zipFile.getInputStream(ze)
                    zis = new ZipInputStream(ins)
                    ZipEntry zentry = zis.getNextEntry()

                    while ( zentry != null )
                    {
                        if (zentry.getName().endsWith("fastqc_data.txt"))
                        {
                            // println "Reading zentry: ${zentry.getName()}"
                            Scanner sc = new Scanner(zis)
                            parseFastqcData(sc, sample)
                            // println "Reading zentry: ${zentry.getName()} completed"
                        }
                        else
                        {
                            //println "SUBZIP GENERIC FILE: ${zentry.getName()}"
                        }
                        zentry = zis.getNextEntry()
                    }
                    ins.close()
                }
            }

            project.samples.push sample
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace()
        }
        catch (IOException e)
        {
            e.printStackTrace()
        }
        finally
        {
            if( ins != null ) ins.close()
            if( zis != null ) zis.close()
        }

    }

    public static void parseFastqcData(Scanner sc, gfureporter.fastqc.Sample sample)
    {
        StringBuffer all_stats = new StringBuffer()
        while (sc.hasNextLine())
        {
            all_stats << sc.nextLine() << "\n"
        }
        Matcher m = (all_stats =~ /(?ms)>>Basic Statistics(.+?)>>END_MODULE/)
        String base_stats = m[0][1]
        boolean base_pass = (base_stats.replaceAll(/\n.*/,"").stripIndent().trim() == "pass") ? true : false
        String filename   = (base_stats =~ /(?m)Filename\s*(.+?)$/)[0][1].stripIndent().trim()
        int seqs = (base_stats =~ /(?ms).*Total Sequences\s*(\d+).*/)[0][1].toInteger()
        int gc = (base_stats =~ /(?ms).*\%GC\s*(\d+)/)[0][1].toInteger()

        String type = (filename ==~ /.*R2.*/ ? "R2" : "R1")

        sample.read_groups.push(new gfureporter.fastqc.ReadsGroup(
            name: type,
            fileName: filename,
            gc: gc,
            reads: seqs
        ))
    }
}
