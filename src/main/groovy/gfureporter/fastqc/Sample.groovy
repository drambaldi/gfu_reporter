package gfureporter.fastqc

/**
 * @author davide
 * Created: Mon Jan 20 15:17:20 CET 2014
 */

class Sample
{
    public String name
    public ArrayList read_groups

    Sample()
    {
        read_groups = new ArrayList()
    }

    String toString()
    {
        StringBuffer res = new StringBuffer()
        res << "SAMPLE: $name\n"
        res << "\tfiles:\n"
        read_groups.each { read_group ->
            res << "\t\t${read_group.name}\t${read_group.fileName}\t${read_group.gc}\t${read_group.reads}\n"
        }
        res << "\n"
        return res
    }
}

class ReadsGroup
{
    String name
    String fileName
    int    gc
    int    reads
}
